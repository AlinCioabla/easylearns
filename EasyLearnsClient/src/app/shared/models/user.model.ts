export interface User {
    UserId: number;
    FirstName: string;
    LastName: string;
    UserName: string;
    StartedTrainings: number[];
    FinishedTrainings: number[];
}