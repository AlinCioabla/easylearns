import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SearchService {
    private searchBoxValue = new BehaviorSubject<any>('');
    private areTagsFiltered  = new BehaviorSubject<any>(false);

    sendString(key: string) {
        this.sendBool(key === '');
        this.searchBoxValue.next(key);
    }

    getString(): Observable<any> {
        return this.searchBoxValue.asObservable();
    }

    sendBool(bit: boolean) {
        this.areTagsFiltered.next(bit);
    }

    getBool(): Observable<any> {
        return this.areTagsFiltered.asObservable();
    }
}
