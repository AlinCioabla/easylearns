import { Component, OnInit } from '@angular/core';
import { select } from '@angular-redux/store';
import { Subject } from 'rxjs';
import { User } from '../../../shared/models/user.model';
import { Training } from '../../models/training.model';
import { NavbarService } from '../../../core/navbar/navbar.service';
import { TrainingsActionsCreator } from '../../store/trainings.actions';
import { ActivatedRoute } from '@angular/router';
import { TrainingsService } from '../../services/trainings.service';
import { VideoPlayerService } from '../../services/video-player.service';

@Component({
  selector: 'app-my-trainings',
  templateUrl: './my-trainings.component.html'
})

export class MyTrainingsComponent implements OnInit {
  @select(['user', 'currentUser']) public user$: Subject<User>;
  public startedTrainings: Training[] = [];
  public finishedTrainings: Training[] = [];
  public newestTrainings: Training[] = [];
  public isLoading = true;
  levelName = ['indexZeroLevel!', 'Beginner', 'Intermediate', 'Advanced', 'Expert'];

  constructor(
    private actions: TrainingsActionsCreator,
    private srv: TrainingsService,
    public videoPlayerService: VideoPlayerService,
    private navbarService: NavbarService
  ) {
    this.navbarService.show();
  }

  ngOnInit() {
    this.navbarService.show();
    this.actions.fetchTrainings();

    this.srv.getTrainings().subscribe(result => {
      if (result.length > 0) {
        this.user$.subscribe(user => {
          this.isLoading = true;
          if (user) {
            this.isLoading = false;
            this.startedTrainings = result.filter(training => user.StartedTrainings.find(t => t === training.TrainingId) > 0);
            this.finishedTrainings = result.filter(training => user.FinishedTrainings.find(t => t === training.TrainingId) > 0);
            this.newestTrainings = result.slice(length - 5);
          }
        });
      }
    });
  }

  floor(x: number) {
    return Math.floor(x);
  }
}
