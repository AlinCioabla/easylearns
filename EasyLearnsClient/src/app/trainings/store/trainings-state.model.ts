import { Training } from '../models/training.model';
import { PlayerSettings } from '../../shared/app-state/player-state.model';

export interface ITrainingsState {
    collection: Training[];
    tags: string[];
    playerSettings: PlayerSettings;
}

export const TRAINING_INITIAL_STATE: ITrainingsState = {
    collection: null,
    tags: null,
    playerSettings: new PlayerSettings()
};
