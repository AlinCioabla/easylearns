﻿namespace EasyLearns.DataModel
{
    public class UserLoginModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
