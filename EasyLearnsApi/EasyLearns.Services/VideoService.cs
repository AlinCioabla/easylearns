﻿namespace EasyLearns.Services
{
    public class VideoService
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        
        public string GetLink(int id)
        {
            return _unitOfWork.TrainingMovieRepository.GetMovie(id);
        }
    }
}
