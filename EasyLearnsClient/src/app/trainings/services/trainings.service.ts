import { AppHelper } from './../../shared/helpers/app.helper';
import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject, Subject, Observable, throwError } from 'rxjs';
import { Training } from '../models/training.model';
import { select } from '@angular-redux/store';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { Tag } from '../models/tag.model';

@Injectable()
export class TrainingsService {
    @select(['trainings', 'collection']) public trainings$: Subject<Training[]>;
    public trainingsSource$ = new BehaviorSubject([]);

    constructor(private http: HttpClient) {
        this.trainings$.subscribe(result => {
            if (result) {
                this.trainingsSource$.next(result);
            }
        });
    }

    public filterTrainings(name: string, tags: string[], rating: number, level: number, durationInterval: number) {
        this.trainings$.subscribe(result => {
            if (result) {
                let newArray = result;

                if (level !== undefined) {
                    newArray = newArray.filter(x => x.Level === level);
                }

                if (rating !== undefined) {
                    newArray = newArray.filter(x => Math.floor(x.Rating) === rating);
                }

                if (durationInterval !== undefined) {
                    const lowerBound = [0, 10800, 25200, 46800, 79200];
                    const upperBound = [10800, 25200, 46800, 79200, 3603600];

                    newArray = newArray.filter(
                        x => x.Duration >= lowerBound[durationInterval] && x.Duration < upperBound[durationInterval]
                    );
                }

                if (tags !== undefined && tags.length !== 0) {
                    newArray = newArray.filter(x => {
                        for (let i = 0; i < tags.length; i++) {
                            if (x.Tags.includes(tags[i])) {
                                return true;
                            }
                        }
                        return false;
                    });
                }

                if (name !== '' && name !== undefined) {
                    newArray = newArray.filter(x => x.Name.toLowerCase().includes(name.toLowerCase()));
                }

                this.trainingsSource$.next(newArray);
            }
        });
    }

    public getTrainings() {
        return this.http.get<Training[]>(AppHelper.TrainingsUrl)
            .pipe(
                catchError(this.handleError)
            );
    }

    public getTrainingById(id: number) {
        const url = `${AppHelper.TrainingsUrl}/${id}`;
        return this.http.get<Training>(url)
            .pipe(
                catchError(this.handleError)
            );
    }

    public getTags(): Observable<Tag[]> {
        return this.http.get<Tag[]>(AppHelper.TagsUrl).pipe(
            tap(results => { results.sort((t1, t2) => this.compareStrings(t1.Name, t2.Name)); }),
            catchError(this.handleError)
        );
    }

    public compareStrings(s1: string, s2: string) {
        if (s1 === s2) {
            return 0;
        }

        if (s1 < s2) {
            return -1;
        }

        return 1;
    }

    public giveRating(trainingId: number, rating: number) {
        const param = trainingId * 10 + rating;
        this.http.post<number>(`${AppHelper.RatingUrl}/${param}`, null).subscribe(res => res);
    }

    public deleteProgress(trainingId: number) {
        this.http.delete(`${AppHelper.TrainingsUrl}/${trainingId}`).subscribe(res => res);
        console.log(`${AppHelper.TrainingsUrl}/${trainingId}`);
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
          'Something bad happened; please try again later.');
      }
}
