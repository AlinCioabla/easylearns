export interface MovieProgress {
    MovieId: number;
    MovieCompleted: boolean;
    ProgressDetails: number;
}
