﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using System.Web.Http;
using EasyLearns.Common.Utils;
using EasyLearns.DataModel;
using EasyLearns.Services;

namespace EasyLearns.API.Controllers
{
	//[EnableCors("*", "*", "*")]
	public class AuthenticateController : ApiController
	{
		private readonly UserService _userService = new UserService();

		[HttpPost]
		[Route(Constants.Routes.Authenticate)]
		public IHttpActionResult Authenticate([FromBody] UserLoginModel userLoginModel)
		{
			UserModel dbUser = _userService.Get(userLoginModel.Name);
			if (dbUser == null)
			{
				return BadRequest("Invalid username");
			}

			// Initialize the hashing algorithm
			byte[] keyBytes = Encoding.ASCII.GetBytes(WebConfigurationManager.AppSettings["HashKey"]);
			HMACSHA256 hasher = new HMACSHA256(keyBytes);

			// Compute the hash for the provided user password
			byte[] passwordBytes = Encoding.ASCII.GetBytes(userLoginModel.Password);
			byte[] hashedPassword = hasher.ComputeHash(passwordBytes);
			string encryptedPassword = Convert.ToBase64String(hashedPassword);

			if (dbUser.Password != encryptedPassword)
			{
				return BadRequest("Invalid password");
			}

			if (dbUser.RegistrationAccepted == 0)
			{
				return BadRequest("Your registration was not accepted yet. Please be patient");
			}

			var tokenBytes = Encoding.ASCII.GetBytes(dbUser.UserId.ToString() + ';' + dbUser.UserName + ';' + dbUser.FirstName + ';' + dbUser.LastName);
			var encodedToken = Convert.ToBase64String(tokenBytes);

			return Ok(encodedToken);
		}

		[HttpPost]
		[Route(Constants.Routes.Register)]
		public IHttpActionResult Register([FromBody] UserRegistrationModel userRegisterModel)
		{
			// Check if user is already registered
			UserModel dbUser = _userService.Get(userRegisterModel.UserName);
			if (dbUser != null)
			{
				return BadRequest("Username is already in use.");
			}

			// Initialize the hashing algorithm
			byte[] keyBytes = Encoding.ASCII.GetBytes(WebConfigurationManager.AppSettings["HashKey"]);
			HMACSHA256 hasher = new HMACSHA256(keyBytes);

			// Compute the hash for the provided user password
			byte[] passwordBytes = Encoding.ASCII.GetBytes(userRegisterModel.Password);
			byte[] hashedPassword = hasher.ComputeHash(passwordBytes);
			string encryptedPassword = Convert.ToBase64String(hashedPassword);

			UserModel newUser = new UserModel()
			{
				FirstName = userRegisterModel.FirstName,
				LastName = userRegisterModel.LastName,
				Password = encryptedPassword,
				UserName = userRegisterModel.UserName,
				RegistrationAccepted = 0,
			};

			_userService.Add(newUser);

			return Ok("Registration successful. Please wait for the account activation");
		}
	}
}