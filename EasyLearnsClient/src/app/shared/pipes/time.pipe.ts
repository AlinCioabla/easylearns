import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'time'
})
export class TimePipe implements PipeTransform {
    transform(value: number, args?: any) {
        if (!value) {
            return null;
        }

        value = Math.floor(value);

        let answer = '';
        const hours = Math.floor(value / 3600);
        const minutes = Math.floor((value - hours * 3600) / 60);
        const seconds = value % 60;

        if (hours  >  9) {
            answer += Math.floor(hours / 10);
        }
        if (hours) {
            answer += hours % 10 + ':';
        }
        if (minutes > 9 || hours) {
            answer += Math.floor(minutes / 10);
        }
        answer += minutes % 10  +  ':' + Math.floor(seconds / 10);

        return answer + seconds % 10;
    }
}
