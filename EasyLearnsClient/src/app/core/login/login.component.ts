import { Component, OnInit } from '@angular/core';
import { LoginModel } from '../../shared/models/login.model';
import { Router } from '@angular/router';
import { AuthenticateService } from '../../shared/authentication/authenticate.service';
import { AppHelper } from '../../shared/helpers/app.helper';
import { RegisterModel } from 'src/app/shared/models/register.model';
import { HttpErrorResponse } from '@angular/common/http';

export enum Mode {
  eRegister = 'register',
  eLogin = 'login'
};

export enum MessageType {
  eWarning = 'warning',
  eSuccess = 'success'
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  title = 'Easy learns';

  Mode = Mode;
  MessageType = MessageType;

  mode = Mode.eLogin;

  loginModel: LoginModel = new LoginModel();
  registerModel: RegisterModel = new RegisterModel();

  showMessagebox: Boolean = false;
  messageType: MessageType = MessageType.eSuccess;
  message: string = '';

  repeatedPassword: string = null;

  operationInProgress: Boolean = false;

  public constructor(
    private authService: AuthenticateService,
    private router: Router) { }

  public ngOnInit() {
    localStorage.removeItem('AuthToken');
  }

  public onLogin() {
    this.operationInProgress = true;
    this.hideMessageBox();
    this.authService.login(this.loginModel).subscribe(result => {
      AppHelper.SetAuthToken(result);
      this.router.navigate(['my-trainings']);
    }, (httpError: HttpErrorResponse) => {
      this.operationInProgress = false;
      this.showMessage(MessageType.eWarning, httpError.error.Message);
    });
  }

  public onRegister() {
    if (this.repeatedPassword !== this.registerModel.password) {
      this.showMessage(MessageType.eWarning, 'Passwords do not match');
      return;
    }

    this.operationInProgress = true;
    this.hideMessageBox();
    this.authService.register(this.registerModel).subscribe(result => {
      this.mode = Mode.eLogin;
      this.operationInProgress = false;
      this.showMessage(MessageType.eSuccess, 'Register was successful. The account will be activated by an administrator soon');
    }, (httpError: HttpErrorResponse) => {
      this.operationInProgress = false;
      this.showMessage(MessageType.eWarning, httpError.error.Message);
    });
  }

  public showMessage(type: MessageType, message: string) {
    this.messageType = type;
    this.message = message;
    this.showMessagebox = true;
  }

  public hideMessageBox() {
    this.message = '';
    this.showMessagebox = false;
  }
}
