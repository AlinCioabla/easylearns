﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using EasyLearns.Common.Utils;
using EasyLearns.DataModel;
using EasyLearns.Services;

namespace EasyLearns.API.Controllers
{
    public class TrainingsController : BaseController
    {
        private readonly TrainingsService _trainingsService = new TrainingsService();
        private readonly UserService _userService = new UserService();
        private readonly CommentsService _commentsService = new CommentsService();

        [HttpGet]
        [Route(Constants.Routes.Trainings)]
        [ResponseType(typeof(List<TrainingModel>))]
        public IHttpActionResult Get()
        {
			List<TrainingModel> result = _trainingsService.GetAllTrainings();
            if (result != null)
            {
                return Ok(result);
            }
            return NotFound();
        }

        [HttpGet]
        [Route(Constants.Routes.TrainingById)]
        public IHttpActionResult Get([FromUri] int id)
        {
            var trainings = _trainingsService.GetById(id, CurrentUser.UserId);
            if (trainings != null)
            {
                trainings.Comments = _commentsService.GetComments(trainings.TrainingId, 0);
                return Ok(trainings);
            }
            return NotFound();
        }

        [HttpPost]
        [Route(Constants.Routes.TrainingProgress)]
        public void UpdateProgress([FromBody] UserTrainingProgressModel progress)
        {
            _userService.UpdateProgress( progress, CurrentUser.UserId);
        }

        [HttpDelete]
        [Route(Constants.Routes.TrainingById)]
        public void Delete([FromUri] int id)
        {
            _userService.DeleteProgress(CurrentUser.UserId, id);
        }
    }
}
