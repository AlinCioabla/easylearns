﻿using EasyLearns.DataAccess.DataModel;

namespace EasyLearns.DataAccess.Repositories
{
    public class TagsRepository : GenericRepository<Tag>
    {
        private readonly EasyLearnsDb _context;

        public TagsRepository(EasyLearnsDb context) : base(context)
        {
            _context = context;
        }

        //public List<string> GetAll()
        //{
        //    return _context.Tags.Select(Name).ToList();
        //}
    }
}
