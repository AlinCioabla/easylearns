
export function callApiMiddleware({ dispatch, getState }) {
    return next => action => {
        const {
            type,
            callApi,
            shouldCallApi,
            payload = {},
            success = function () { }
        } = action;

        if (!shouldCallApi) {
            return next(action);
        }

        if (typeof callApi !== 'function') {
            throw new Error('Expected callApi to be a function');
        }
        if (typeof shouldCallApi !== 'function') {
            throw new Error('Expected shouldCallApi to be a function');
        }
        if (!shouldCallApi(getState())) {
            return;
        }

        return callApi().subscribe(response => {
            success(response);
            return dispatch({
                type: type,
                payload: response || null
            });
        });
    };
}
