import { NavbarService } from './../../../core/navbar/navbar.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TrainingsService } from '../../services/trainings.service';
import { Training } from '../../models/training.model';
import {Location} from '@angular/common';

@Component({
  selector: 'app-training-landing-page',
  templateUrl: './training-landing-page.component.html'
})
export class TrainingLandingPageComponent implements OnInit {
  trainingId: number;
  training: Training;
  rating: number;

  constructor(
    private route: ActivatedRoute,
    public trainingsService: TrainingsService,
    private location: Location,
    private navbarService: NavbarService
  ) {
    this.navbarService.show();
    this.trainingId = Number(this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit() {
    this.trainingsService.getTrainingById(this.trainingId).subscribe(result => {
      this.training = result;
      this.rating = this.training.Rating;
    });
  }

  floor(x: number) {
    return Math.floor(x);
  }

  rate() {
    this.trainingsService.giveRating(this.training.TrainingId, this.rating);
  }

  resetProgress() {
    this.trainingsService.deleteProgress(this.training.TrainingId);
    this.training.MoviesCompleted = 0;
    this.trainingsService.getTrainingById(this.training.TrainingId).subscribe(res =>
      this.training = res
    );
  }

  backClicked() {
    this.location.back();
  }

  constructTrainingCoverImage(training: Training): string {
    const src = 'data:image/jpg;base64,' + atob(training.Image);
    return src;
  }

}
