import { User } from "../../shared/models/user.model";

export interface Comment{
    ParentCommentId: number;
    Text: string;
    User: User;
    Replys: Comment[];
}
