export class AppHelper {
    public static Url = 'http://localhost:53382';
    public static LoginUrl = `${AppHelper.Url}${'/authenticate'}`;
    public static RegisterUrl = `${AppHelper.Url}${'/register'}`;
    public static TrainingsUrl = `${AppHelper.Url}${'/trainings'}`;
    public static TagsUrl = `${AppHelper.Url}${'/tags'}`;
    public static VideoUrl = `${AppHelper.Url}${'/video'}`;
    public static ProgressUrl = `${AppHelper.Url}${'/trainings/progress'}`;
    public static RatingUrl = `${AppHelper.Url}${'/rate'}`;
    public static UserUrl = `${AppHelper.Url}${'/user'}`;

    public static SetAuthToken(token: string) {
        localStorage.setItem('AuthToken', token);
    }
    public static GetAuthToken(): string {
        return localStorage.getItem('AuthToken');
    }
}
