import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { select } from '@angular-redux/store';
import { HttpClient } from '@angular/common/http';
import { AppHelper } from '../helpers/app.helper';
import { User } from '../models/user.model';

@Injectable()
export class UserService {

    constructor(private http: HttpClient) {}

    getUser() {
        return this.http.get<User>(AppHelper.UserUrl);
    }

}
