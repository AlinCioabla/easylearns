import { NgModule } from '@angular/core';
import { NgReduxModule, DevToolsExtension, NgRedux } from '@angular-redux/store';
import { rootReducer } from './shared/app-state/app.reducers';
import { IAppState, APP_INITIAL_STATE } from './shared/app-state/app-state.model';
import { environment } from '../environments/environment';
import { callApiMiddleware } from './shared/app-state/app.middleware';
import { createLogger } from 'redux-logger';

@NgModule({
    imports: [NgReduxModule]
})
export class AppStoreModule {
    constructor(private store: NgRedux<IAppState>, private devTools: DevToolsExtension) {
        let middleware = [callApiMiddleware];

        if (!environment.production) {
            middleware = [...middleware, createLogger()];
        }

        store.configureStore(
            rootReducer,
            APP_INITIAL_STATE,
            middleware,
            devTools.isEnabled() ? [devTools.enhancer()] : []
        );
    }
}
