﻿using System.Linq;
using EasyLearns.DataAccess.DataModel;

namespace EasyLearns.DataAccess.Repositories
{
    public class TrainingsRepository : GenericRepository<Training>
    {
        private readonly EasyLearnsDb _context;

        public TrainingsRepository(EasyLearnsDb context) : base(context)
        {
            _context = context;
        }


        public TrainingMovie GetMovie(int MovieID)
        {
            return _context.TrainingMovies.Where(x=>x.MovieID==MovieID).FirstOrDefault();
        }

        public bool? VideoState(int UserID, int MovieID)
        {
            UserTrainingProgress progress = _context.UserTrainingProgresses.Where(x => x.MovieId == MovieID && x.UserId == UserID).FirstOrDefault();
            if (progress == null)
                return false;
            return progress.MovieCompleted;
        }

        public int? Progress(int UserID, int MovieID)
        {
            UserTrainingProgress progress = _context.UserTrainingProgresses.Where(x => x.MovieId == MovieID && x.UserId == UserID).FirstOrDefault();
            if (progress == null)
                return 0;
            return progress.ProgressDetails;
        }

    }
}