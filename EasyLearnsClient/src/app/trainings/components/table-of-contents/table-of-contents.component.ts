import { Router } from '@angular/router';
import { Training } from './../../models/training.model';
import { VideoPlayerService } from './../../services/video-player.service';
import { Component, OnInit, Input } from '@angular/core';
import { TrainingMovie } from '../../models/movie.model';
import { TrainingChapter } from '../../models/chapter.model';


@Component({
  selector: 'app-table-of-contents',
  templateUrl: './table-of-contents.component.html'
})
export class TableOfContentsComponent implements OnInit {
  @Input('training') training: Training;
  @Input('minWidth') minWidth: boolean;

  constructor(public videoPlayerService: VideoPlayerService, private router: Router) { }

  ngOnInit() {
  }

  updatePlaylist(training: Training, movie: TrainingMovie, chapter: TrainingChapter) {
    this.videoPlayerService.updatePlaylist(training, movie, chapter);
    this.videoPlayerService.updateProgress(0, movie.IsCompleted);
    this.router.navigate([`/trainings/${training.TrainingId}/${movie.MovieId}/${chapter.SessionId}`]);
  }

}
