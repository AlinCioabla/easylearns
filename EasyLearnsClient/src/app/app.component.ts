import { Component } from '@angular/core';
import { AuthenticateService } from './shared/authentication/authenticate.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(public authSrv: AuthenticateService) { }
}
