﻿using System.Collections.Generic;
using System.Linq;
using EasyLearns.DataAccess.DataModel;

namespace EasyLearns.DataAccess.Repositories
{
    public class ProgressRepository : GenericRepository<UserTrainingProgress>
    {
        private readonly EasyLearnsDb _context;

        public ProgressRepository(EasyLearnsDb context) : base(context)
        {
            _context = context;
        }


        public UserTrainingProgress GetProgress(int userId, int movieId)
        {
            return _context.UserTrainingProgresses
                .Where(x => x.UserId == userId && x.MovieId == movieId)
                .FirstOrDefault();
        }

        public List<UserTrainingProgress> GetTrainingProgress(int userID, int trainingId)
        {
            return _context.UserTrainingProgresses
                .Where(x => x.UserId == userID && x.TrainingId == trainingId)
                .ToList();
        }
    }
}
