﻿using System;
using System.Collections.Generic;
using System.Linq;
using EasyLearns.Common.Utils;
using EasyLearns.DataAccess.DataModel;
using EasyLearns.DataModel;

namespace EasyLearns.Services
{
    public class UserService : BaseService
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();


        public UserModel Get(string userName)
        {
            var dbUser = _unitOfWork.UserRepository
                .Get(userModel => userModel.Username == userName)
                .FirstOrDefault();
            
            if (dbUser?.UserID > 0)
            {
               return new UserModel
                {
                    UserId = dbUser.UserID,
                    UserName = dbUser.Username,
                    FirstName = dbUser.FirstName,
                    LastName = dbUser.LastName,
					Password = dbUser.Password,
					RegistrationAccepted = dbUser.RegistrationAccepted
                };
            }

            return null;
        }

	    public UserModel Get(UserModel user)
	    {
		    var dbUser = _unitOfWork.UserRepository
			    .Get(userModel =>
				    userModel.Username == user.UserName &&
				    userModel.FirstName == user.FirstName &&
				    userModel.LastName == user.LastName)
				.FirstOrDefault();

		    if (dbUser?.UserID > 0)
		    {
			    return new UserModel
			    {
				    UserId = dbUser.UserID,
				    UserName = dbUser.Username,
				    FirstName = dbUser.FirstName,
				    LastName = dbUser.LastName,
					RegistrationAccepted = dbUser.RegistrationAccepted
			    };
		    }

		    return null;
	    }

		public void UpdateProgress( UserTrainingProgressModel progress, int userId )
        {
            UserTrainingProgress userProgress = _unitOfWork.ProgressRepository.GetProgress(userId, progress.MovieId);
            if (userProgress == null)
            {
                _unitOfWork.ProgressRepository.Insert(new UserTrainingProgress
                {
                    UserId = userId,
                    LastOpened = DateTime.Now,
                    MovieId = progress.MovieId,
                    TrainingId = _unitOfWork.TrainingsRepository.Get(x => x.TrainingChapters.FirstOrDefault(y => y.TrainingMovies.FirstOrDefault(z => z.MovieID == progress.MovieId) != null) != null).Select(t => t.TrainingID).FirstOrDefault(),
                    MovieCompleted = progress.MovieCompleted,
                    ProgressDetails = progress.ProgressDetails
                });
                _unitOfWork.SaveChanges();
                userProgress = _unitOfWork.ProgressRepository.GetProgress(userId, progress.MovieId);
            }
            else
            {
                userProgress.LastOpened = DateTime.Now;
                userProgress.MovieCompleted = progress.MovieCompleted;
                userProgress.ProgressDetails = progress.ProgressDetails;
            }

            bool trcmpl = false;

            TrainingMovie movie = _unitOfWork.TrainingMovieRepository.GetById(progress.MovieId);

            UserEnrollment userEnrollment = _unitOfWork.UserEnrollmentsRepository.Get(x => x.UserID == userId && x.TrainingID == movie.TrainingChapter.Training.TrainingID).FirstOrDefault();
            if (userEnrollment == null)
            {
                _unitOfWork.UserEnrollmentsRepository.Insert(new UserEnrollment
                {
                    TrainingID = movie.TrainingChapter.Training.TrainingID,
                    UserID = userId,
                    Rating = 0,
                });

                _unitOfWork.SaveChanges();
            }

            if (userProgress.MovieCompleted)
            {
                int totalMovies = 0;
                int completedMovies = 0;
                List<UserTrainingProgressModel> progresst = _unitOfWork.ProgressRepository
                    .Get(x=>x.UserId==userId)
                    .Select(x=> new UserTrainingProgressModel {
                        MovieId = x.MovieId,
                        MovieCompleted = x.MovieCompleted,
                        ProgressDetails = x.ProgressDetails
                    } )
                    .ToList();
                foreach (var chapter in userProgress.Training.TrainingChapters)
                {
                    totalMovies += chapter.TrainingMovies.Count;
                    foreach(var moviet in chapter.TrainingMovies)
                    {
                        var y = progresst.Where(x => x.MovieId == moviet.MovieID).FirstOrDefault();
                        if (y != null)
                        if (y.MovieCompleted)
                            completedMovies++;
                    }
                }
                if (completedMovies == totalMovies)
                    trcmpl = true;
            }


            _unitOfWork.UserEnrollmentsRepository.Get(x => x.UserID == userId && x.TrainingID == movie.TrainingChapter.Training.TrainingID).FirstOrDefault().Completed = trcmpl;




            _unitOfWork.SaveChanges();

        }


        public void DeleteProgress(int userId,int trainingId)
        {
            var deletable = _unitOfWork.ProgressRepository.Get(x => x.UserId == userId && x.TrainingId == trainingId).ToList();
            if (deletable != null)
                foreach(var del in deletable)
                    _unitOfWork.ProgressRepository.Delete(del.TrainingProgressID);

            _unitOfWork.SaveChanges();
        }

        public void UpdateRating(int userId, int trainingId, int stars)
        {
            UserEnrollment userEnrollment = _unitOfWork.UserEnrollmentsRepository.Get(x => x.UserID == userId && x.TrainingID == trainingId).FirstOrDefault();
            if (userEnrollment == null)
            {
                userEnrollment = new UserEnrollment { TrainingID = trainingId, UserID = userId };
                _unitOfWork.UserEnrollmentsRepository.Insert(userEnrollment);
            }
            userEnrollment.Rating = stars;
            _unitOfWork.SaveChanges();
            List<UserEnrollment> allUsers = _unitOfWork.UserEnrollmentsRepository.Get(x=> x.TrainingID == trainingId && x.Rating>0).ToList();
            int? rating = 0;
            foreach (UserEnrollment user in allUsers)
                rating += user.Rating;
            decimal trainingRating = Convert.ToDecimal(rating) / allUsers.Count;
            Training training = _unitOfWork.TrainingsRepository.Get(x => x.TrainingID == trainingId).FirstOrDefault();
            training.Rating = trainingRating;
            _unitOfWork.SaveChanges();
        }

        public List<int> GetUserTrainings(int userId, bool completed)
        {
            return _unitOfWork.UserEnrollmentsRepository
                .Get(x => x.UserID == userId && x.Completed == completed)
                .Select(y => y.TrainingID)
                .Take(Constants.Generic.NoOfTrainings)
                .ToList();
        }

        public void Add(UserModel user)
        {
            _unitOfWork.UserRepository.Insert(new User
            {
	            Username = user.UserName,
	            FirstName = user.FirstName,
	            LastName = user.LastName,
				Password = user.Password,
				RegistrationAccepted = user.RegistrationAccepted
            });
            _unitOfWork.SaveChanges();
        }

    }
}
