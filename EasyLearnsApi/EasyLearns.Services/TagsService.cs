﻿using System.Collections.Generic;
using System.Linq;
using EasyLearns.DataAccess.DataModel;
using EasyLearns.DataModel;

namespace EasyLearns.Services
{
    public class TagsService : BaseService
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        public List<TagsModel> GetAllTags()
        {
            List<Tag> tags = _unitOfWork.TagsRepository.Get().ToList();
            List<TagsModel> tagsModel = tags.Select(x=> new TagsModel {
                TagID = x.TagID,
                Name = x.Name })
                .ToList();
            return tagsModel;
        }
    }
}
