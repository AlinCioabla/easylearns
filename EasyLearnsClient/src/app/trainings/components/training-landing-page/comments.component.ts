import { Component, OnInit, Input } from "@angular/core";

@Component({
    selector: 'app-comments',
    templateUrl: './comments.component.html'
})
export class CommentsComponent {
    @Input('comments') comments: Comment[];
    @Input('level') level: number;
}