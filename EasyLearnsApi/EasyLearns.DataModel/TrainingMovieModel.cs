﻿namespace EasyLearns.DataModel
{
    public class TrainingMovieModel
    {
        public string FileName { get; set; }
        public int MovieId { get; set; }
        public int? Duration { get; set; }

        public bool? IsCompleted { get; set; }
        public int? MovieProgress { get; set; }
    }
}