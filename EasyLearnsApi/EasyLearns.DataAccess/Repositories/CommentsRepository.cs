﻿using EasyLearns.DataAccess.DataModel;

namespace EasyLearns.DataAccess.Repositories
{
    public class CommentsRepository : GenericRepository<Comment>
    {
        private readonly EasyLearnsDb _context;

        public CommentsRepository(EasyLearnsDb context) : base(context)
        {
            _context = context;
        }
    }
}
