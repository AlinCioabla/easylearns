﻿using EasyLearns.DataAccess.DataModel;

namespace EasyLearns.DataAccess.Repositories
{
    public class UserEnrollmentsRepository : GenericRepository<UserEnrollment>
    {
        private readonly EasyLearnsDb _context;

        public UserEnrollmentsRepository(EasyLearnsDb context) : base(context)
        {
            _context = context;
        }

    }
}
