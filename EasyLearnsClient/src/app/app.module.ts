import { SafeHtml } from './shared/pipes/safeHtml.pipe';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './core/login/login.component';
import { NavbarComponent } from './core/navbar/navbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSelectModule } from '@angular/material/select';
import { AuthenticateService } from './shared/authentication/authenticate.service';
import { AuthGuardService } from './shared/authentication/authenticate.guard';
import { AppStoreModule } from './app-store.model';
import { TrainingsActionsCreator } from './trainings/store/trainings.actions';
import { TrainingsService } from './trainings/services/trainings.service';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSliderModule } from '@angular/material/slider';
import { AuthInterceptor } from './shared/authentication/auth.interceptor';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TimePipe } from 'src/app/shared/pipes/time.pipe';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TrainingDetailsComponent } from './trainings/components/training-details/training-details.component';
import { TrainingsListComponent } from './trainings/components/trainings-list/trainings-list.component';
import { VideoPlayerComponent } from './trainings/components/video-player/video-player.component';
import { TrainingLandingPageComponent } from './trainings/components/training-landing-page/training-landing-page.component';
import { TableOfContentsComponent } from './trainings/components/table-of-contents/table-of-contents.component';
import { PageNotFoundComponent } from './core//page-not-found/page-not-found.component';
import { RatingPipe } from 'src/app/shared/pipes/rating.pipe';
import { UserActionsCreator } from './shared/app-state/user.actions';
import { UserService } from './shared/services/user.service';
import {RatingModule} from 'primeng/rating';
import { MyTrainingsComponent } from './trainings/components/my-trainings/my-trainings.component';
import { TrainingCardComponent } from './trainings/components/training-card/training-card.component';
import { CommentsComponent } from './trainings/components/training-landing-page/comments.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'training-landing-page/:id', component: TrainingLandingPageComponent, canActivate: [AuthGuardService] },
  { path: 'trainings/:id/:movieId/:chapterId', component: TrainingDetailsComponent, canActivate: [AuthGuardService] },
  { path: 'trainings/:id', component: TrainingDetailsComponent, canActivate: [AuthGuardService] },
  { path: 'trainings', component: TrainingsListComponent, canActivate: [AuthGuardService] },
  { path: 'my-trainings', component: MyTrainingsComponent, canActivate: [AuthGuardService] },
  { path: 'training-card', component: TrainingCardComponent, canActivate: [AuthGuardService] },
  { path: 'comments', component: CommentsComponent, canActivate: [AuthGuardService] },
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    TrainingsListComponent,
    TrainingDetailsComponent,
    VideoPlayerComponent,
    TimePipe,
    RatingPipe,
    SafeHtml,
    TrainingLandingPageComponent,
    TableOfContentsComponent,
    PageNotFoundComponent,
    MyTrainingsComponent,
    TrainingCardComponent,
    CommentsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppStoreModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatInputModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatSelectModule,
    MatChipsModule,
    MatTableModule,
    MatExpansionModule,
    FlexLayoutModule,
    MatTabsModule,
    MatPaginatorModule,
    MatListModule,
    MatSidenavModule,
    MatProgressBarModule,
    MatSliderModule,
    MatCardModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    RatingModule
  ],
  providers: [AuthenticateService, AuthGuardService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,
  },
    TrainingsActionsCreator,
    UserActionsCreator,
    UserService,
    TrainingsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
