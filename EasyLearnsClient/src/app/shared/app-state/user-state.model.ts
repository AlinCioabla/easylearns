import { User } from '../models/user.model';

export interface IUserState {
    currentUser: User;
}

export const USER_INITIAL_STATE: IUserState = {
    currentUser: {
        UserId: 0,
        FirstName: " ",
        LastName: " ",
        UserName: " ",
        StartedTrainings: null,
        FinishedTrainings: null
    }
};
