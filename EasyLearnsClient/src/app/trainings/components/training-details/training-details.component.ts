import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Training } from '../../models/training.model';
import { TrainingsService } from '../../services/trainings.service';
import { VideoPlayerService } from '../../services/video-player.service';
import { NavbarService } from '../../../core/navbar/navbar.service';

@Component({
  selector: 'app-training-details',
  templateUrl: './training-details.component.html',
  styleUrls: ['./training-details.component.css']
})
export class TrainingDetailsComponent implements OnInit, OnDestroy {
  private trainingId: number;

  panelOpenState = false;
  isOpen = false;
  training: Training;
  timeout;
  showControls = true;

  constructor(
    private route: ActivatedRoute,
    private trainingsService: TrainingsService,
    public videoPlayerService: VideoPlayerService,
    private navbarService: NavbarService
  ) {
    this.trainingId = Number(this.route.snapshot.paramMap.get('id'));
    this.navbarService.hide();
  }

  ngOnInit() {
    this.trainingsService.getTrainingById(this.trainingId).subscribe(result => {
      this.training = result;
    });
  }
  ngOnDestroy() {
    this.navbarService.show();
  }

  hideControls() {
    clearTimeout(this.timeout);
    this.showControls = true;
    this.timeout = setTimeout(() => this.showControls = false, 3000);
  }
}
