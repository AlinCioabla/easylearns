﻿using System.Web.Http;
using EasyLearns.Common.Utils;
using EasyLearns.Services;

namespace EasyLearns.API.Controllers
{
    public class VideoController : BaseController
    {
        private readonly VideoService _videoService = new VideoService();

        [HttpGet]
        [Route(Constants.Routes.Video)]
        public IHttpActionResult Get([FromUri] int id)
        {
            var result = _videoService.GetLink(id);
            if (result != null)
            {
                return Ok(result);
            }
            return NotFound();
        }
    }
}