import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material';
import { SearchService } from '../../../shared/SearchService';
import { Training } from '../../models/training.model';
import { TrainingsActionsCreator } from '../../store/trainings.actions';
import { TrainingsService } from '../../services/trainings.service';
import { NavbarService } from '../../../core/navbar/navbar.service';
import { Subject } from 'rxjs/internal/Subject';
import { Tag } from '../../models/tag.model';
import { select } from '@angular-redux/store';

@Component({
  selector: 'app-trainings-list',
  templateUrl: './trainings-list.component.html',
  styleUrls: ['./trainings-list.component.css']
})

export class TrainingsListComponent implements OnInit {
  @select(['trainings', 'tags']) public tags$: Subject<Tag[]>;

  selectedTag;
  selectedRating;
  selectedLevel;
  selectedDuration;

  trainingsAll: Training[] = [];
  trainings: Training[] = [];

  pageSize;
  pageIndex = 0;
  pageSizeOptions: number[];

  searchKey: string;
  length: number;
  levelName = ['indexZeroLevel!', 'Beginner', 'Intermediate', 'Advanced', 'Expert'];
  tagList: string[];
  searchBoxValue = '';
  rating: number[];
  tags = [];
  filteredTags = [];
  filtered = false;

  constructor(
    private actions: TrainingsActionsCreator,
    public srv: TrainingsService,
    public messageService: SearchService,
    public navbarService: NavbarService) { }

  ngOnInit() {
    this.navbarService.show();
    this.actions.fetchTrainings();
    this.actions.fetchTags();

    this.srv.trainingsSource$.subscribe(result => {
      this.trainingsAll = result;

      this.length = this.trainingsAll.length;
      this.pageSize = this.length;
      this.pageIndex = 0;

      this.UpdatePaginator();
      this.UpdateTrainingsList();
    });

    this.srv.getTags().subscribe(result => {
      for (let i = 0; i < result.length; i++) {
        this.tags.push(result[i].Name);
      }
    });
    this.messageService.getString().subscribe(message => { this.searchBoxValue = message; });
    this.messageService.getBool().subscribe(bit => {
      this.filtered = bit;

      if (bit === true) {
        this.clearFilters();
      }
  });
  }

  floor(x: number) {
    return Math.floor(x);
  }

  onPage(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.UpdateTrainingsList();
  }

  UpdateTrainingsList() {
    this.trainings = this.trainingsAll.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
  }

  UpdatePaginator() {
    this.pageSizeOptions = [this.length];

    for (let i = 2; 5 * i < this.length; i += 2) {
      this.pageSizeOptions.push(5 * i);
    }
    if (this.pageSizeOptions.length > 1) {
      this.pageSize = this.pageSizeOptions[1];
    }
  }

  filterTags() {
    if (this.filtered === false) {
      this.filteredTags = this.tags.filter(x => {
        for (let i = 0; i < this.trainings.length; i++) {
          for (let j = 0; j < this.trainings[i].Tags.length; j++) {
            if (this.trainings[i].Tags[j] === x) {
              return true;
            }
          }
        }
        return false;
      });

      this.messageService.sendBool(true);
    }

    return this.filteredTags;
  }

  clearFilters() {
    this.selectedTag = this.selectedRating = this.selectedLevel = this.selectedDuration = undefined;

    return 1;
  }
}
