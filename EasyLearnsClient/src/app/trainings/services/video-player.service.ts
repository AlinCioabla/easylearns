import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Training } from '../models/training.model';
import { TrainingMovie } from '../models/movie.model';
import { TrainingChapter } from '../models/chapter.model';
import { MovieProgress } from '../models/progress.model';
import { AppHelper } from '../../shared/helpers/app.helper';

@Injectable({
  providedIn: 'root'
})
export class VideoPlayerService {
  currentMovie: TrainingMovie;
  currentMovieIndex: number;
  currentChapter: TrainingChapter;
  currentChapterIndex: number;
  progress: MovieProgress;
  videoUrl;

  constructor(private http: HttpClient) { }

  updatePlaylist(training: Training, subchapter: TrainingMovie, chapter: TrainingChapter) {

    this.currentMovie = subchapter;
    this.currentChapter = chapter;
    this.currentChapterIndex = training.TrainingChapters.indexOf(this.currentChapter);
    this.currentMovieIndex = this.currentChapter.TrainingMovies.indexOf(this.currentMovie);
    this.setVideoSrc(this.currentMovie.MovieId);
  }

  setVideoSrc(id: number) {
    this.videoUrl = this.getVideoById(id).subscribe(res => this.videoUrl = res);
  }

  getVideoById(id: number) {
    const url = `${AppHelper.VideoUrl}/${id}`;
    return this.http.get<Training>(url)
      .pipe(
        catchError(this.handleError)
      );
  }

  updateProgress(time: number, isCompleted: boolean) {

    this.progress = {
      MovieId: this.currentMovie.MovieId,
      MovieCompleted: isCompleted,
      ProgressDetails: Math.round(time)
    };

    this.http.post<MovieProgress>(AppHelper.ProgressUrl, this.progress).subscribe(res => res);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
}

}
