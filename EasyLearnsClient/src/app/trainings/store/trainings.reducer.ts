import { TrainingActionTypes } from './trainings.actions';
import { ITrainingsState, TRAINING_INITIAL_STATE } from './trainings-state.model';
import { StandardAction, createReducer, IAppState } from '../../shared/app-state/app-state.model';

class TrainingsReducer {
    [TrainingActionTypes.FETCH_TRAININGS_SUCCESS](state: ITrainingsState,
        action: StandardAction): ITrainingsState {
        return {
            ...state,
            collection: action.payload
        };
    }

    [TrainingActionTypes.FETCH_TAGS_SUCCESS](state: ITrainingsState,
        action: StandardAction): ITrainingsState {
        return {
            ...state,
            tags: action.payload
        };
    }

    [TrainingActionTypes.CHANGE_SPEED_SUCCESS](state: ITrainingsState, action: StandardAction) {
        return {
            ...state,
            playerSettings: {
                ...state.playerSettings,
                speed: action.payload
            }
        };
    }

    [TrainingActionTypes.CHANGE_VOLUME_SUCCESS](state: ITrainingsState, action: StandardAction) {
        return {
            ...state,
            playerSettings: {
                ...state.playerSettings,
                volume: action.payload
            }
        };
    }
}

export const trainingsReducer = createReducer(TRAINING_INITIAL_STATE, new TrainingsReducer());
