﻿using System;
using System.Collections.Generic;

namespace EasyLearns.DataModel
{
    public class TrainingModel
    {
        public int TrainingId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Duration { get; set; }
        public int Level { get; set; }
        public decimal Rating { get; set; }
        public DateTime? TrainingDate { get; set; }
        public byte[] Image { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ChangedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? ChangedBy { get; set; }

        public int LastOpenedMovie { get; set; }

        public int MoviesCompleted { get; set; }
        public int TotalMovies { get; set; }

        public List<CommentsModel> Comments { get; set; }

        public List<TrainingChaptersModel> TrainingChapters { get; set; }
        public List<string> Tags { get; set; }
    }
}