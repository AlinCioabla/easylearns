import { TrainingChapter } from './chapter.model';

export interface Training {
    TrainingId: number;
    Name: string;
    Description: string;
    Duration: number;
    Level: number;
    Rating: number;
    LastOpenedMovie: number;
    Tags: string[];
    MoviesCompleted: number;
    TotalMovies: number;
    TotalChapters: number;
    Image: string;
    TrainingDate: Date;
    TrainingChapters: TrainingChapter[];
    Comments: Comment[];
    CreationDate: Date;
    ChangedDate: Date;
    CreatedBy: number;
    ChangedBy: number;
}

