import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html'
})
export class PageNotFoundComponent implements OnInit {

  actualTime = 5;

  constructor(public router: Router) { }

  ngOnInit() {
    this.startCountDown(4);
  }
  startCountDown(seconds) {
    const interval = setInterval(() => {
      this.actualTime = seconds;
      seconds--;
      if (seconds < 0 ) {

        this.router.navigateByUrl('/trainings');

        clearInterval(interval);
      }
    }, 1000);
  }
}
