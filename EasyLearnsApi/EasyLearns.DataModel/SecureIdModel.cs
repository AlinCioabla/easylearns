﻿namespace EasyLearns.DataModel
{
    //{"data":{"sid":"DhymM4kcEQAJI1420LSN003831"},"success":true}
    public class SecureIdModel
    {
        public Data data { get; set; }
        public bool success { get; set; }
    }

    public class Data
    {
        public string sid { get; set; }
    }

}
