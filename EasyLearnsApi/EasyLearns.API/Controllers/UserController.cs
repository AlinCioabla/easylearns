﻿using System.Web.Http;
using EasyLearns.Common.Utils;
using EasyLearns.DataModel;
using EasyLearns.Services;

namespace EasyLearns.API.Controllers
{
    public class UserController : BaseController
    {
        private readonly UserService _userService = new UserService();


        [HttpGet]
        [Route(Constants.Routes.User)]
        public IHttpActionResult Get()
        {
            UserModel userModel = new UserModel
            {
                StartedTrainings = _userService.GetUserTrainings(CurrentUser.UserId, false),
                FinishedTrainings = _userService.GetUserTrainings(CurrentUser.UserId, true),
                UserId = CurrentUser.UserId,
                UserName = CurrentUser.UserName,
                FirstName = CurrentUser.FirstName,
                LastName = CurrentUser.LastName
            };
            return Ok(userModel);
        }

        [HttpPost]
        [Route(Constants.Routes.Rating)]
        public void UpdateRating([FromUri] int data)
        {
            int trainingId = data / 10;
            int stars = data % 10;
            _userService.UpdateRating(CurrentUser.UserId, trainingId, stars);
        }

    }
}