export class TrainingMovie {
    MovieId: number;
    FileName: string;
    MovieProgress: number;
    IsCompleted: boolean;
    Duration: number;
    Seen = false;
}
