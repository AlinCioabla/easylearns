import { Injectable } from '@angular/core';
import { LoginModel } from '../models/login.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AppHelper } from '../helpers/app.helper';
import { User } from '../models/user.model';
import { RegisterModel } from 'src/app/shared/models/register.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {
  constructor(private http: HttpClient) { }

  public login(user: LoginModel): Observable<string> {
    return this.http.post<string>(AppHelper.LoginUrl, user);
  }

  public register(user: RegisterModel): Observable<string> {
    return this.http.post<string>(AppHelper.RegisterUrl, user);
  }

  public getUser(): Observable<User> {
    return this.http.get<User>(AppHelper.UserUrl);
  }

  public isAuthenticated() {
    if (AppHelper.GetAuthToken()) {
      return true;
    } else {
      return false;
    }
  }
}
