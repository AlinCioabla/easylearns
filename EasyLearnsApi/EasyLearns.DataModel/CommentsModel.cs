﻿using System.Collections.Generic;

namespace EasyLearns.DataModel
{
    public class CommentsModel
    {
        public int? ParentCommentId { get; set; }
        public string Text { get; set; }
        public List<CommentsModel> Replys { get; set; }
        public UserModel User { get; set; }
    }
}
