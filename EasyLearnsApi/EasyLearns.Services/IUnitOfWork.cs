﻿namespace EasyLearns.Services
{
    public interface IUnitOfWork
    {
        void SaveChanges();
    }
}
