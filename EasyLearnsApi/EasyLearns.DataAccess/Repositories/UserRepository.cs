﻿using System.Collections.Generic;
using System.Linq;
using EasyLearns.DataAccess.DataModel;

namespace EasyLearns.DataAccess.Repositories
{
    public class UserRepository: GenericRepository<User>
    {
        private readonly EasyLearnsDb _context;

        public UserRepository(EasyLearnsDb context) : base(context)
        {
            _context = context;
        }
        
        public List<User> GetUsers()
        {
            return _context.Users.ToList();
        }

        public void Add(User user)
        {
            _context.Users.Add(user);
        }

    }
}