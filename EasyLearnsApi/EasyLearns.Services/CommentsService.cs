﻿using System.Collections.Generic;
using System.Linq;
using EasyLearns.DataModel;

namespace EasyLearns.Services
{
    public class CommentsService
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        
        public List<CommentsModel> GetComments(int trainingId, int commentId)
        {
            List<CommentsModel> comments = _unitOfWork.CommentsRepository
                .Get(x => x.TrainingID == trainingId && commentId == (x.ParentID != null ? x.ParentID : 0))
                .Select(y => new CommentsModel
                {
                    ParentCommentId = y.ParentID,
                    Text = y.Comment1,
                    User = new UserModel
                    {
                        FirstName = y.User.FirstName,
                        LastName = y.User.LastName,
                        UserName = y.User.Username
                    },
                    Replys = GetComments(trainingId, y.CommentID)
                })
                .ToList();
            return comments;
        }
    }
}
