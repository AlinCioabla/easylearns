﻿using System;
using EasyLearns.DataAccess.DataModel;
using EasyLearns.DataAccess.Repositories;

namespace EasyLearns.Services
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        public EasyLearnsDb Context { get; }

        public UnitOfWork()
        {
            if (Context == null)
            {
                Context = new EasyLearnsDb();
            }
        }

        // Add all the repository handles here
        private TrainingsRepository _trainingsRepository;
        private UserRepository _authenticateRepository;
        private ProgressRepository _progressRepository;
        private TagsRepository _tagsRepository;
        private UserRepository _userRepository;
        private TrainingMovieRepository _trainingMovieRepository;
        private UserEnrollmentsRepository _userEnrollmentsRepository;
        private CommentsRepository _commentsRepository;

        // Add all the repository getters here
        public TrainingsRepository TrainingsRepository
        {
            get
            {
                if (_trainingsRepository == null)
                {
                    _trainingsRepository = new TrainingsRepository(Context);
                }
                return _trainingsRepository;
            }
        }

        public UserRepository AuthenticateRepository
        {
            get
            {
                if (_authenticateRepository == null)
                {
                    _authenticateRepository = new UserRepository(Context);
                }
                return _authenticateRepository;
            }
        }

        public ProgressRepository ProgressRepository
        {
            get
            {
                if (_progressRepository == null)
                {
                    _progressRepository = new ProgressRepository(Context);
                }
                return _progressRepository;
            }
        }

        public TagsRepository TagsRepository
        {
            get
            {
                if (_tagsRepository == null)
                {
                    _tagsRepository = new TagsRepository(Context);
                }
                return _tagsRepository;
            }
        }

        public UserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(Context);
                }
                return _userRepository;
            }
        }

        public TrainingMovieRepository TrainingMovieRepository
        {
            get
            {
                if (_trainingMovieRepository == null)
                {
                    _trainingMovieRepository = new TrainingMovieRepository(Context);
                }
                return _trainingMovieRepository;
            }
        }

        public UserEnrollmentsRepository UserEnrollmentsRepository
        {
            get
            {
                if (_userEnrollmentsRepository == null)
                {
                    _userEnrollmentsRepository = new UserEnrollmentsRepository(Context);
                }
                return _userEnrollmentsRepository;
            }
        }

        public CommentsRepository CommentsRepository
        {
            get
            {
                if (_commentsRepository == null)
                {
                    _commentsRepository = new CommentsRepository(Context);
                }
                return _commentsRepository;
            }
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}