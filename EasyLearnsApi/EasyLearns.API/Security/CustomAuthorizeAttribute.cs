﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;
using EasyLearns.API.Controllers;
using EasyLearns.Common.Utils;
using EasyLearns.DataModel;
using EasyLearns.Services;

namespace EasyLearns.API.Security
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly UserService _userService = new UserService();

        public object Request { get; private set; }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            if (!actionContext.Request.Headers.Contains("token"))
            {
                throw new Exception(ErrorMessages.TokenError);
            }
            var token = actionContext.Request.Headers.GetValues("token").FirstOrDefault();
            var user = DecodeToken(token);

            var baseController = (BaseController)actionContext.ControllerContext.Controller;

            UserModel userModel = _userService.Get(user);
            if (userModel != null)
            {
                baseController.CurrentUser = user;
                return true;
            }
            //throw new Exception(Common.Utils.ErrorMessages.UserError);
            return false;
        }

        public UserModel DecodeToken(string token)
        {
            string result = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(token));

            string[] parsing = new string[4];
            int j = 0;

            for (int i = 0; i < result.Length; i++)
            {
                if (result[i] == ';')
                    j++;
                else
                    parsing[j] += result[i];
            }

            int x = 0;

            if (Int32.TryParse(parsing[0], out x))
            {
                x = Int32.Parse(parsing[0]);
            }
            else
                return null;

            return new UserModel { UserId = x, UserName = parsing[1], FirstName = parsing[2], LastName = parsing[3] };
        }
    }
}