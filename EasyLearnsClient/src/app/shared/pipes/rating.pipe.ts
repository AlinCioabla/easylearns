import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'rating'
})
export class RatingPipe implements PipeTransform {
  transform(value, args: string[]): any {
    const res = [];
    value = Math.floor(value);
    for (let i = 0; i < value; i++) {
        res.push(i);
      }
      return res;
  }
}
