﻿using System.Web.Http;
using System.Web.Http.Cors;
using EasyLearns.API.Security;
using EasyLearns.DataModel;

namespace EasyLearns.API.Controllers
{
    [CustomAuthorize]
    [EnableCors("*", "*", "*")]
    public class BaseController: ApiController
    {
        public UserModel CurrentUser{ get; set; }
    }
}