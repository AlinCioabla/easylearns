﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using EasyLearns.Common.Utils;
using EasyLearns.Services;

namespace EasyLearns.API.Controllers
{
    public class TagsController : BaseController
    {
        private readonly TagsService _tagsService = new TagsService();

        [HttpGet]
        [Route(Constants.Routes.Tags)]
        [ResponseType(typeof(List<string>))]
        public IHttpActionResult Get()
        {
            var result = _tagsService.GetAllTags();
            if (result != null)
                return Ok(result);
            return NotFound();
        }
    }
}
