﻿using System.Collections.Generic;

namespace EasyLearns.DataModel
{
    public class GroupsModel
    {
        public string Name { get; set; }
        public List<int> Permissions { get; set; }
    }
}
