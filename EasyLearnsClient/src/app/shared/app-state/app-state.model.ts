import { ITrainingsState, TRAINING_INITIAL_STATE } from '../../trainings/store/trainings-state.model';
import { IUserState, USER_INITIAL_STATE } from './user-state.model';

/* App State*/
export interface IAppState {
    trainings: ITrainingsState;
    user: IUserState;
}

export interface StandardAction {
    type: string;
    payload: any;
}

/* App Initial State*/
export const APP_INITIAL_STATE: IAppState = {
    trainings: TRAINING_INITIAL_STATE,
    user: USER_INITIAL_STATE
};


export function createReducer<TStateSlice, TReducer>(initialState: TStateSlice, reducer: TReducer) {
    return function (state: TStateSlice = initialState, action: any): TStateSlice {
        if (reducer[action.type]) {
            return reducer[action.type](state, action);
        } else {
            return state;
        }
    };
}
