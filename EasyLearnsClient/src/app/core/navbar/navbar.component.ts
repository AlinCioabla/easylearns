import { Router } from '@angular/router';
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { TrainingsService } from '../../trainings/services/trainings.service';
import { SearchService } from '../../shared/SearchService';
import { NavbarService } from './navbar.service';
import { MatAutocompleteTrigger } from '@angular/material';
import { UserActionsCreator } from '../../shared/app-state/user.actions';
import { Subject } from 'rxjs';
import { select } from '@angular-redux/store';
import { User } from '../../shared/models/user.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  @select(['user', 'currentUser']) public user$: Subject<User>;

  tagList: string[];
  searchBoxValue = '';
  filteredOptions = [];
  trainingsAllNames = [];

  @ViewChild(MatAutocompleteTrigger) autocomplete: MatAutocompleteTrigger;

  constructor(
    private tsrv: TrainingsService,
    private router: Router,
    private messageService: SearchService,
    public navbarService: NavbarService,
    public userActions: UserActionsCreator) { }

  ngOnInit() {
    this.userActions.fetchUser();
    this.tsrv.getTrainings().subscribe(result => {
      this.trainingsAllNames = [];
      for (let i = 0; i < result.length; i++) {
        this.trainingsAllNames.push(result[i].Name.toLowerCase());
      }
    });

    this.messageService.getString().subscribe(val => {
      this.searchBoxValue = val;
    });
  }

  trainingsPage() {
    this.router.navigate(['/trainings']);
  }

  mytrainingsPage() {
    this.router.navigate(['my-trainings']);
  }

  logout() {
    localStorage.removeItem('AuthToken');
    this.router.navigate(['/']);
  }

  handle($event, searchBoxValue) {
    if ($event.key === 'Enter') {
      this.executeSearch(searchBoxValue);
    } else {
      this.updateOptions(searchBoxValue);
    }
  }

  executeSearch(searchBoxValue: string) {
    this.messageService.sendString(searchBoxValue);
    this.tsrv.filterTrainings(searchBoxValue, undefined, undefined, undefined, undefined);
    this.autocomplete.closePanel();
  }

  updateOptions(searchBoxValue: string) {
    searchBoxValue = searchBoxValue.toLowerCase();
    this.filteredOptions = this.trainingsAllNames.filter(x => x.includes(searchBoxValue));
  }
}
