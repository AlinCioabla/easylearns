import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AppHelper } from '../helpers/app.helper';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private router: Router) { }

    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = AppHelper.GetAuthToken();
        request = request.clone({
            setHeaders: {
                'token': `${token}`,
                'Content-Type': 'application/json'
            }
        });

        return next.handle(request).pipe(
            catchError((err: any) => {
                if (err.status === 401) {
                    this.router.navigate(['/']);
                }
                throw (err);
            })
        );
    }
}
