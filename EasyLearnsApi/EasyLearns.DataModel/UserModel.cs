﻿using System.Collections.Generic;

namespace EasyLearns.DataModel
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
		public int RegistrationAccepted { get; set; }
        public List<int> StartedTrainings { get; set; }
        public List<int> FinishedTrainings { get; set; }
    }
}
