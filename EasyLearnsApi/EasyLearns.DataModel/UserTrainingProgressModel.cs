﻿namespace EasyLearns.DataModel
{
    public class UserTrainingProgressModel
    {
        public int MovieId { get; set; }
        public bool MovieCompleted { get; set; }
        public int ProgressDetails { get; set; }
    }
}
