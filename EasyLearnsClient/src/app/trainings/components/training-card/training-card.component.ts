import { VideoPlayerService } from '../../services/video-player.service';
import { NavbarService } from '../../../core/navbar/navbar.service';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Training } from '../../models/training.model';

@Component({
  selector: 'app-training-card',
  templateUrl: './training-card.component.html',
})

export class TrainingCardComponent {
  public trainingId: number;
  @Input('training') training: Training;
  levelName = ['indexZeroLevel!', 'Beginner', 'Intermediate', 'Advanced', 'Expert'];

  constructor(
    public videoPlayerService: VideoPlayerService,
    private navbarService: NavbarService
  ) {
    this.navbarService.show();
  }

  floor(x: number) {
    return Math.floor(x);
  }

  constructTrainingCoverImage(training: Training): string {
    const src = 'data:image/jpg;base64,' + atob(training.Image);
    return src;
  }
}
