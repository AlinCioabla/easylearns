﻿using System.Collections.Generic;

namespace EasyLearns.DataModel
{
    public class TrainingChaptersModel
    {
        public string FolderName { get; set; }
        public int SessionId { get; set; }
        public int TrainingId { get; set; }
        public int MoviesCompleted { get; set; }
        public int TotalMovies { get; set; }
        public List<TrainingMovieModel> TrainingMovies { get; set; }
    }
}