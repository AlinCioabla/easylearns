﻿using System;
using System.Collections.Generic;
using System.Linq;
using EasyLearns.DataAccess.DataModel;
using EasyLearns.DataModel;

namespace EasyLearns.Services
{
	public class TrainingsService : BaseService
	{
		private readonly UnitOfWork _unitOfWork = new UnitOfWork();

		public List<TrainingModel> GetAllTrainings()
		{
			var trainingsList = _unitOfWork.TrainingsRepository
					.Get()
					.Select(training => new TrainingModel
					{
						TrainingId = training.TrainingID,
						Duration = training.Duration,
						Description = training.Description,
						Name = training.Name,
						Level = training.Level,
						Rating = training.Rating,
						TrainingDate = training.TrainingDate,
						ChangedDate = training.ChangedDate,
						CreatedBy = training.CreatedBy,
						ChangedBy = training.ChangedBy,
						Image = training.Image,
						Tags = training.Tags.Select(x => x.Name).ToList()
					}).ToList();

			return trainingsList;
		}

		public TrainingModel GetById(int trainingId, int userId)
		{
			TrainingModel showableCourse;
			int totalMovies = 0, viewedMovies = 0;

			var course = _unitOfWork.TrainingsRepository.GetById(trainingId);
			if (course != null)
			{
				List<UserTrainingProgress> progress = _unitOfWork.ProgressRepository.GetTrainingProgress(userId, trainingId);

				showableCourse = new TrainingModel
				{
					TrainingId = course.TrainingID,
					Duration = course.Duration,
					Description = course.Description,
					Name = course.Name,
					Level = course.Level,
					TrainingDate = course.TrainingDate,
					ChangedDate = course.ChangedDate,
					CreatedBy = course.CreatedBy,
					ChangedBy = course.ChangedBy,
					Image = course.Image,
					Tags = course.Tags.Select(x => x.Name).ToList(),
					LastOpenedMovie = course.UserTrainingProgresses
						.Where(x => x.UserId == userId)
						.OrderByDescending(y => y.LastOpened)
						.Select(z => z.MovieId)
						.FirstOrDefault(),
					TrainingChapters = course.TrainingChapters.OrderBy(zz => zz.Index).Select(x => new TrainingChaptersModel
					{
						FolderName = x.FolderName,
						SessionId = x.SessionID,
						TrainingId = x.TrainingID,
						TrainingMovies = x.TrainingMovies.OrderBy(zzz => zzz.Index).Select(t => new TrainingMovieModel
						{
							FileName = t.FileName,
							MovieId = t.MovieID,
							Duration = t.Duration,
							IsCompleted = progress.Where(z => z.MovieId == t.MovieID).Select(zz => zz.MovieCompleted).FirstOrDefault(),
							MovieProgress = progress.Where(z => z.MovieId == t.MovieID).Select(zz => zz.ProgressDetails).FirstOrDefault()
						}).ToList(),
						TotalMovies = x.TrainingMovies.Count
					}).ToList()
				};
				var uer = _unitOfWork.UserEnrollmentsRepository.Get(x => x.UserID == userId && x.TrainingID == trainingId).FirstOrDefault();
				if (uer != null)
					showableCourse.Rating = Convert.ToDecimal(uer.Rating);
				else
					showableCourse.Rating = 0;


				if (progress != null)
					for (int i = 0; i < progress.Count; i++)
					{
						if (progress[i].MovieCompleted == true)
						{
							int SelectedSession = _unitOfWork.TrainingsRepository.GetMovie(progress[i].MovieId).SessionID;
							showableCourse.TrainingChapters.Where(x => x.SessionId == SelectedSession).FirstOrDefault().MoviesCompleted++;
						}
					}

				for (int i = 0; i < showableCourse.TrainingChapters.Count; i++)
				{

					viewedMovies += showableCourse.TrainingChapters[i].MoviesCompleted;
					totalMovies += showableCourse.TrainingChapters[i].TotalMovies;

				}
				showableCourse.TotalMovies = totalMovies;
				showableCourse.MoviesCompleted = viewedMovies;
			}
			else
				return null;
			return showableCourse;
		}
	}
}
