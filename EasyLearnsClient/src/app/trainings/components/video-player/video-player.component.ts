import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { VideoPlayerService } from '../../services/video-player.service';
import { TrainingChapter } from '../../models/chapter.model';
import { TrainingMovie } from '../../models/movie.model';
import { Training } from '../../models/training.model';
import { select } from '@angular-redux/store';
import { PlayerSettings } from '../../../shared/app-state/player-state.model';
import { Subject } from 'rxjs/internal/Subject';
import { TrainingsActionsCreator } from '../../store/trainings.actions';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnInit {
  @select(['trainings', 'playerSettings']) public playerSettings$: Subject<PlayerSettings>;

  @Input('training') training: Training;
  @Input('showControls') showControls: Training;
  @Input('content') content;
  @Input('drawer') drawer;

  panelOpenState = false;
  isOpen = false;
  chapters: TrainingChapter[];
  savedTime: number;
  showSpeedSlider = false;
  showVolumeSlider = false;
  document: any = window.document;

  constructor(
    public videoPlayerService: VideoPlayerService,
    private trainingActions: TrainingsActionsCreator,
    private cdRef: ChangeDetectorRef,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    let chapter: TrainingChapter;
    let movie: TrainingMovie;
    let chapterSaved: TrainingChapter;
    let movieSaved: TrainingMovie;

    const paramChapterId = Number(this.route.snapshot.paramMap.get('chapterId'));
    if (paramChapterId) {
      chapterSaved = this.training.TrainingChapters.find(c => c.SessionId === paramChapterId);
    } else {
      this.training.TrainingChapters.forEach(c => {
        if (c.TrainingMovies.find(m => m.MovieId === this.training.LastOpenedMovie)) {
          chapterSaved = c;
        }
      });
    }
    if (chapterSaved) {
      chapter = chapterSaved;
    } else {
      chapter = this.training.TrainingChapters[0];
    }

    const paramMovieId = Number(this.route.snapshot.paramMap.get('movieId'));
    if (paramMovieId) {
      movieSaved = chapter.TrainingMovies.find(m => m.MovieId === paramMovieId);
    } else {
      movieSaved = chapter.TrainingMovies.find(m => m.MovieId === this.training.LastOpenedMovie);
    }

    if (movieSaved) {
      movie = movieSaved;

    } else {
      movie = chapter.TrainingMovies[0];
    }
    this.savedTime = movie.MovieProgress;
    this.updatePlaylist(movie, chapter);
  }

  setSpeed(video) {
    this.playerSettings$.subscribe(res => video.playbackRate = res.speed);
  }

  setVolume(video) {
    this.playerSettings$.subscribe(res => video.volunme = res.volume);
  }

  seek(evt, progressBarDiv, progressBar, video) {
    if (this.drawer.opened) {
      progressBar.value = (evt.clientX - this.drawer._width) / progressBarDiv.offsetWidth * 100;
      video.currentTime = video.duration * progressBar.value / 100;
    } else {
      progressBar.value = evt.clientX / progressBarDiv.offsetWidth * 100;
      video.currentTime = video.duration * progressBar.value / 100;
    }
  }

  updatePlaylist(subchapter: TrainingMovie, chapter: TrainingChapter) {
    this.videoPlayerService.updatePlaylist(this.training, subchapter, chapter);
  }

  updateProgress(currentTime: number) {
    if (Math.abs(currentTime - this.savedTime) > 5) {
      this.savedTime = currentTime;
      this.videoPlayerService.updateProgress(this.savedTime, this.videoPlayerService.currentMovie.IsCompleted);
    }
  }

  completedVideo() {
    this.videoPlayerService.updateProgress(0, true);
    this.videoPlayerService.currentMovie.Seen = true;
    this.videoPlayerService.currentChapter.MoviesCompleted += 1;
  }

  hideSlider() {
    this.showSpeedSlider = false;
    this.showVolumeSlider = false;
  }

  changeSpeed(slider) {
    this.trainingActions.changeSpeed(slider.displayValue);
  }

  changeVolume(slider) {
    this.trainingActions.changeVolume(slider.displayValue / 100);
  }

  volume(video) {
    if (video.volume !== 0) {
      video.volume = 0;
    } else {
      video.volume = 1;
    }
  }

  nextMovie(video) {
    if (this.videoPlayerService.currentMovieIndex === this.videoPlayerService.currentChapter.TrainingMovies.length - 1) {
      if (this.videoPlayerService.currentChapterIndex + 1 > this.training.TrainingChapters.length - 1) {
        video.pause();
      } else {
        this.videoPlayerService.currentChapter = this.training.TrainingChapters[this.videoPlayerService.currentChapterIndex + 1];
        const nextMovie = this.videoPlayerService.currentChapter.TrainingMovies[0];
        const nextChapter = this.training.TrainingChapters[this.videoPlayerService.currentChapterIndex + 1];
        this.videoPlayerService.updatePlaylist(this.training, nextMovie, nextChapter);
      }
    } else {
      const nextMovie = this.videoPlayerService.currentChapter.TrainingMovies[this.videoPlayerService.currentMovieIndex + 1];
      this.videoPlayerService.updatePlaylist(this.training, nextMovie, this.videoPlayerService.currentChapter);
    }
  }

  previousMovie(video) {
    if (this.videoPlayerService.currentMovieIndex === 0) {
      if (this.videoPlayerService.currentChapterIndex - 1 < 0) {
        video.pause();
      } else {
        this.videoPlayerService.currentChapter = this.training.TrainingChapters[this.videoPlayerService.currentChapterIndex - 1];
        const previousMovie =
          this.videoPlayerService.currentChapter.TrainingMovies[this.videoPlayerService.currentChapter.TrainingMovies.length - 1];
        const previousChapter = this.training.TrainingChapters[this.videoPlayerService.currentChapterIndex - 1];
        this.videoPlayerService.updatePlaylist(this.training, previousMovie, previousChapter);
      }
    } else {
      const previousMovie = this.videoPlayerService.currentChapter.TrainingMovies[this.videoPlayerService.currentMovieIndex - 1];
      this.videoPlayerService.updatePlaylist(this.training, previousMovie, this.videoPlayerService.currentChapter);
    }

  }

  playPause(video) {
    if (video.paused) {
      video.play();
    } else {
      video.pause();
    }
  }

  fullscreen() {
    if (!this.document.fullscreenElement &&    // alternative standard method
      !this.document.mozFullScreenElement && !this.document.webkitFullscreenElement) {  // current working methods
      if (this.document.documentElement.requestFullscreen) {
        this.document.documentElement.requestFullscreen();
      } else if (this.document.documentElement.mozRequestFullScreen) {
        this.document.documentElement.mozRequestFullScreen();
      } else if (this.document.documentElement.webkitRequestFullscreen) {
        this.document.documentElement.webkitRequestFullscreen();
      }
    } else {
      if (this.document.cancelFullScreen) {
        this.document.cancelFullScreen();
      } else if (this.document.mozCancelFullScreen) {
        this.document.mozCancelFullScreen();
      } else if (this.document.webkitCancelFullScreen) {
        this.document.webkitCancelFullScreen();
      }
    }
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  replay(video) {
    video.currentTime -= 5;
  }

  forward(video) {
    video.currentTime += 5;
  }

}
