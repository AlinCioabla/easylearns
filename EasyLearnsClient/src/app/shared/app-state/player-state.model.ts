export class PlayerSettings {
    speed: number;
    volume: number;

    constructor() {
        this.speed = 1;
        this.volume = 1;
    }
}
