﻿using System;
using System.Linq;
using EasyLearns.Common.Utils;
using EasyLearns.DataAccess.DataModel;

namespace EasyLearns.DataAccess.Repositories
{
    public class TrainingMovieRepository : GenericRepository<TrainingMovie>
    {
        private readonly EasyLearnsDb _context;

        public TrainingMovieRepository(EasyLearnsDb context) : base(context)
        {
            _context = context;
        }

        public string GetMovie(int movieId)
        {
            string path = string.Empty;
            TrainingMovie movie = _context.TrainingMovies.Where(x => x.MovieID == movieId).FirstOrDefault();

            if (movie != null)
                if (!string.IsNullOrWhiteSpace(movie.PlayURL))
                    path = movie.PlayURL;
                else
                {
                    string fullPath = movie.FullPath.Trim().Replace(@"\", "/");
                    Uri myUri = new Uri("http:"+fullPath);
                    string host = myUri.Host;  // host is "www.contoso.com"

                    path = Constants.Links.MediaRootPath + fullPath.Replace(host, string.Empty).Replace(@"///",string.Empty);

                }
            else
                throw new Exception(ErrorMessages.MovieIdError);
            return path;
        }

    }
}
