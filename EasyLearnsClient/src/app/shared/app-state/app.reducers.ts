import { combineReducers } from 'redux';
import { IAppState } from './app-state.model';
import { trainingsReducer } from '../../trainings/store/trainings.reducer';
import { userReducer } from './user.reducer';

export const appReducer = combineReducers<IAppState>({
    trainings: trainingsReducer,
    user: userReducer
});

export const rootReducer = (state, action) => {
    return appReducer(state, action);
};
