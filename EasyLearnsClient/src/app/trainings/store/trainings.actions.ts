import { dispatch } from '@angular-redux/store';
import { IAppState } from '../../shared/app-state/app-state.model';
import { TrainingsService } from '../services/trainings.service';
import { Injectable } from '@angular/core';

export class TrainingActionTypes {
    static readonly FETCH_TRAININGS_SUCCESS = 'FETCH_TRAININGS_SUCCESS';
    static readonly FETCH_TAGS_SUCCESS = 'FETCH_TAGS_SUCCESS';
    static readonly CHANGE_SPEED_SUCCESS = 'CHANGE_SPEED_SUCCESS';
    static readonly CHANGE_VOLUME_SUCCESS = 'CHANGE_VOLUME_SUCCESS';
}

@Injectable()
export class TrainingsActionsCreator {

    constructor(private srv: TrainingsService) { }

    @dispatch()
    fetchTrainings() {
        return {
            type: TrainingActionTypes.FETCH_TRAININGS_SUCCESS,
            shouldCallApi: (state: IAppState) => {
                return !state.trainings ||
                    !state.trainings.collection;
            },
            callApi: () => {
                return this.srv.getTrainings();
            }
        };
    }

    @dispatch()
    fetchTags() {
        return {
            type: TrainingActionTypes.FETCH_TAGS_SUCCESS,
            shouldCallApi: (state: IAppState) => {
                return !state.trainings ||
                    !state.trainings.tags;
            },
            callApi: () => {
                return this.srv.getTags();
            }
        };
    }

    @dispatch()
    changeSpeed(speed: string) {
        return {
            type: TrainingActionTypes.CHANGE_SPEED_SUCCESS,
            payload: speed
        };
    }

    @dispatch()
    changeVolume(volume: number) {
        return {
            type: TrainingActionTypes.CHANGE_VOLUME_SUCCESS,
            payload: volume
        };
    }
    
}
