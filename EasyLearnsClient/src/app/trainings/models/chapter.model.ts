import { TrainingMovie } from './movie.model';

export interface TrainingChapter {
    SessionId: number;
    FolderName: string;
    TrainingId: number;
    TrainingMovies: TrainingMovie[];
    MoviesCompleted: number;
    TotalMovies: number;
}
