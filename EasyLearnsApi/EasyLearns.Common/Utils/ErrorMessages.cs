﻿namespace EasyLearns.Common.Utils
{
    public class ErrorMessages
    {
        public const string TokenError = @"There is no Token parameter found in the request header.";
        public const string UserError = @"No user matches those credentials.";
        public const string MovieIdError = @"No movie has been found.";
        public const string EnrollmentError = @"Invalid user - training association.";
    }
}
