﻿namespace EasyLearns.DataModel
{
    public class TagsModel
    {
        public int TagID { get; set; }
        public string Name { get; set; }
    }
}
