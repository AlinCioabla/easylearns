import { StandardAction, createReducer, IAppState } from './app-state.model';
import { USER_INITIAL_STATE, IUserState } from './user-state.model';
import { UserActionTypes } from './user.actions';

class UserReducer {
    [UserActionTypes.FETCH_USER_SUCCESS](state: IUserState,
        action: StandardAction): IUserState {
        return {
            ...state,
            currentUser: action.payload
        };
    }
}

export const userReducer = createReducer(USER_INITIAL_STATE, new UserReducer());
