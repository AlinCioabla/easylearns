﻿using System.Configuration;

namespace EasyLearns.Common.Utils
{
    public class Constants
    {
        public class Routes
        {
            public const string TrainingById = @"trainings/{id}";
            public const string Trainings = @"trainings";
            public const string TrainingProgress = @"trainings/progress";
            public const string Tags = @"tags";
            public const string Authenticate = @"authenticate";
	        public const string Register = @"register";
            public const string Video = @"video/{id}";
            public const string User = @"user";
            public const string StartedTrainings = @"trainings/started";
            public const string Rating = @"rate/{data}";

        }

        public class Links
        {
            public const string DomainUrl = @"http://media.netrom.live";
            public const string PortalPort = @":5000";
            public const string ApplicationInstance = @"/webapi";
            public const string Parameters = "/entry.cgi?api=SYNO.FileStation.Download&version=2&method=download&mode=open&path=/";
            public const string SID = @"http://media.netrom.live:5000/webapi/auth.cgi?api=SYNO.API.Auth&version=3&method=login&account=admin&passwd=netromNAS&session=FileStation&format=sid";
            public const string MediaRootPath = DomainUrl + PortalPort + ApplicationInstance + Parameters;

                //@"http://media.netrom.live:5000/webapi/entry.cgi?api=SYNO.FileStation.Download&version=2&method=download&mode=open&path=/";
        }

        public class Generic
        {
            public const int NoOfTrainings = 5;
            public static string UserGroups => ConfigurationManager.AppSettings["UserGroups"];
        }
    }
}
