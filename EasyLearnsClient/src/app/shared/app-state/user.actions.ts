import { dispatch } from '@angular-redux/store';
import { IAppState } from './app-state.model';
import { Injectable } from '@angular/core';
import { UserService } from '../services/user.service';

export class UserActionTypes {
    static readonly FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
}

@Injectable()
export class UserActionsCreator {

    constructor(private srv: UserService) { }

    @dispatch()
    fetchUser() {
        return {
            type: UserActionTypes.FETCH_USER_SUCCESS,
            shouldCallApi: (state: IAppState) => {
                return true;
            },
            callApi: () => {
                return this.srv.getUser();
            }
        };
    }
}
